angular.module('starter.controllers', [])

.controller('GamesCtrl', function($scope)
{
    $scope.games =
    [
        {
            user: 
            {
                name: "拉莫斯",
                grade: "大一",
                header: "http://www.runoob.com/try/demo_source/nin_logo.jpeg"
            },
            time: "2016年11月5日14:05",
            field: 
            {
                name: "伯纳乌",
                img: "http://www.runoob.com/try/demo_source/nin_logo.jpeg",
            },
            joinedTimes: "0",
            rateOfWinning: "0",
            stake: "20",
            description: "大一新生，娱乐为主。",
            has: "5",
            order: "10",
            state: "正在集结...",
            isDescripHide: true
        },
        {
            user: 
            {
                name: "拉莫斯",
                grade: "大一",
                header: "http://www.runoob.com/try/demo_source/nin_logo.jpeg"
            },
            time: "2016年11月5日14:05",
            field: 
            {
                name: "伯纳乌",
                img: "http://www.runoob.com/try/demo_source/nin_logo.jpeg",
            },
            joinedTimes: "0",
            rateOfWinning: "0",
            stake: "20",
            description: "大一新生，娱乐为主。",
            has: "5",
            order: "10",
            state: "正在集结...",
            isDescripHide: true
        },
        {
            user: 
            {
                name: "拉莫斯",
                grade: "大一",
                header: "http://www.runoob.com/try/demo_source/nin_logo.jpeg"
            },
            time: "2016年11月5日14:05",
            field: 
            {
                name: "伯纳乌",
                img: "http://www.runoob.com/try/demo_source/nin_logo.jpeg",
            },
            joinedTimes: "0",
            rateOfWinning: "0",
            stake: "20",
            description: "大一新生，娱乐为主。",
            has: "5",
            order: "10",
            state: "正在集结...",
            isDescripHide: true
        }
    ];

    $scope.toggleDescrip = function(game)
    {
        game.isDescripHide = !game.isDescripHide;
    };

    $scope.join = function()
    {
        alert("Join!");
    }

    $scope.newGame = function()
    {
        alert("New Game");
    }

})

.controller('ChatsCtrl', function($scope, Chats) {
    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    $scope.chats = Chats.all();
    $scope.remove = function(chat) {
        Chats.remove(chat);
    };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
    $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope)
{
    $scope.settings =
    {
        enableFriends: true
    };
});
